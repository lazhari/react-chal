import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';

it('renders without crashing', () => {
	const div = document.createElement('div');
	ReactDOM.render(<App />, div);

	expect(div.innerHTML).toContain('Date Form');
	expect(div.innerHTML).toContain('Start Date');
	expect(div.innerHTML).toContain('End Date');

	ReactDOM.unmountComponentAtNode(div);
});
