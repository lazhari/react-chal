import React from 'react';

import './Header.css';
export default () => (
	<div className="header">
		<a href="#default" className="logo">
			<img
				src="https://nimbleways.com/home/wp-content/uploads/2017/11/LOGO-NW-1.png"
				alt="Nimble Ways"
			/>
		</a>
	</div>
);
