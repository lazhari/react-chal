import React, { Component } from 'react';
import { Alert, Button, Card, Col, DatePicker, Divider, Form, Row } from 'antd';
import moment from 'moment';

const { Item: FormItem } = Form;

class DateForm extends Component {
	state = {
		duration: ''
	};
	handleSubmit = e => {
		e.preventDefault();
		this.props.form.validateFields((err, dates) => {
			if (!err) {
				const diff = moment(dates.end).diff(moment(dates.start));
				this.setState({
					duration: Math.round(moment.duration(diff).asDays())
				});
				this.props.form.resetFields();
			}
		});
	};
	validateEndDate = (rule, endDate, callback) => {
		const form = this.props.form;
		const diff = moment(endDate).diff(moment(form.getFieldValue('start')));
		const isValidDate = Math.round(moment.duration(diff).asDays()) > 0;
		if (endDate && !isValidDate) {
			callback('The end date it should be greater than the start date!');
		} else {
			callback();
		}
	};
	render() {
		const {
			form: { getFieldDecorator }
		} = this.props;
		return (
			<Card title="Date Form">
				<Form layout="vertical" onSubmit={this.handleSubmit}>
					{this.state.duration && (
						<Row gutter={24}>
							<Col span={24}>
								<Alert
									message={
										<h3>
											The difference between the two date is:{' '}
											{this.state.duration} Days
										</h3>
									}
									type="success"
								/>
								<Divider />
							</Col>
						</Row>
					)}
					<Row gutter={24}>
						<Col span={24}>
							<FormItem label="Start Date">
								{getFieldDecorator('start', {
									rules: [
										{ required: true, message: 'The start date is required!' }
									],
									validateTrigger: 'onSubmit'
								})(
									<DatePicker
										style={{ width: '100%' }}
										size="large"
										format="DD/MM/YYYY"
									/>
								)}
							</FormItem>
						</Col>
					</Row>
					<Row gutter={24}>
						<Col span={24}>
							<FormItem label="End Date">
								{getFieldDecorator('end', {
									rules: [
										{ required: true, message: 'The end date is required!' },
										{ validator: this.validateEndDate }
									],
									validateTrigger: 'onSubmit'
								})(
									<DatePicker
										style={{ width: '100%' }}
										size="large"
										format="DD/MM/YYYY"
									/>
								)}
							</FormItem>
						</Col>
					</Row>
					<Row gutter={24}>
						<Col lg={{ span: 8, offset: 8 }} sm={24}>
							<Button type="primary" htmlType="submit" size="large" block>
								Submit
							</Button>
						</Col>
					</Row>
				</Form>
			</Card>
		);
	}
}

export default Form.create()(DateForm);
