import React, { Component } from 'react';
import { Layout, Row, Col } from 'antd';

import './App.css';
import Header from './components/Header';
import DateFrom from './components/DateForm';

const { Content } = Layout;

class App extends Component {
	render() {
		return (
			<Layout className="layout">
				<Header />
				<Content className="content">
					<Row>
						<Col lg={{ span: 12, offset: 6 }} sm={24}>
							<DateFrom />
						</Col>
					</Row>
				</Content>
			</Layout>
		);
	}
}

export default App;
